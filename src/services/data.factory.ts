import { Data, DataObject } from '@/types/data.type';
import UIData from '@/types/uidata.type';

const setConfirmedTotal = (countryData: Data[]) => { 
    const totalObject: UIData = countryData.reduce(function(prev: Data, current: Data) {
        return (prev.confirmed > current.confirmed) ? prev : current
    });
    return totalObject.confirmed;
};

const getHighestConfirmed = (uiData: UIData[]) => {
    let highestConfirmed = 0;
        uiData.forEach( (country: UIData) => {
            highestConfirmed = highestConfirmed < country.confirmed? country.confirmed : highestConfirmed;
        });
    return highestConfirmed;
};

const sortByHighestConfirmed = (uiData: UIData[])  => {
    uiData.sort((a: UIData, b: UIData) => { 
        return b.confirmed - a.confirmed;
    });
}

const setConfirmedTotals = (data: DataObject) => {
    const countries: string[] = Object.keys(data);
    const uiData: UIData[] = [];

    countries.forEach( (country: string)=> {
        const dataInstance: Data[] = data[country];
        uiData.push ({
            name: country,
            confirmed: setConfirmedTotal(dataInstance)
        });
    });

    sortByHighestConfirmed(uiData);
    return uiData;
}

export { setConfirmedTotals, getHighestConfirmed };