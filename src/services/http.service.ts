import httpClient from './httpClient';

// const endpoint = process.env.VUE_APP_DATA_URL;
const endpoint = 'covid19/timeseries.json';
const mockEndpoint = process.env.VUE_APP_BASE_URL + 'pomber-mock.json';
const environment = process.env.NODE_ENV;

const isLocalTesting = () => { 
    return process.env.NODE_ENV === 'e2e';
}
    
const getData = () => {
    console.log(mockEndpoint);
    const dataSource = isLocalTesting() ? mockEndpoint : endpoint;
    return httpClient.get(dataSource);
}

export { getData }