import axios from 'axios';
const baseUrl = 'https://pomber.github.io/';
const httpClient = axios.create({
    baseURL: baseUrl,
    timeout: 1000,
    headers: {
        "Content-Type": "application/json"
    }
});

export default httpClient;