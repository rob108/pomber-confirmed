import Vue from 'vue'
import Vuex from 'vuex'
import { getData } from '@/services/http.service';

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    data: {}
  },
  mutations: {
    updateData (state, data) {
      state.data = data
    }
  },
  actions: {
    async fetchData({ commit }) {
      try {
          const response = await getData();
          commit('updateData', response.data);
      } catch (error) {
          // TODO: unHappy Deal with
      }    
    }
  },
  getters: {
    data: state => state.data,
  }, 
  modules: {
  }
});

export default store;