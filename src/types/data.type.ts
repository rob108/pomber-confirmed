type Data = {
    date: string;
    confirmed: number;
    deaths: number;
    recovered: number;
}

type DataObject = {
    [key: string]: Data[];
}

export  { Data, DataObject }
