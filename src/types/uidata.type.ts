type UIData = {
    name?: string;
    confirmed: number;
}

export default UIData;
