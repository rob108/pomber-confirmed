type Colours = {
    threshold: number; 
    colour: string;
}

export default Colours;
