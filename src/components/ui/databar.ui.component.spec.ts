import { expect } from "chai";
// import sinon from "sinon";
// import sinonChai from "sinon-chai";
// chai.use(sinonChai);

import { shallowMount } from '@vue/test-utils'
import DataBar from './databar.ui.component.vue'

describe('Data Bar', () => {
    let component: any;
    beforeEach( ()=> {
        component = new DataBar();
    });

    describe('Data Bar - Unit Tests', () => {
        it('setColour()', ()=> {
            component.colour = '';
            const mockColour1 = { threshold: 5, colour: 'green' };
            const mockColour2 = { threshold: 10, colour: 'blue' };
            const mockColour3 = { threshold: 50, colour: 'yellow' };

            component.colours = [
                mockColour1, mockColour2, mockColour3
            ];

            component.width = '25';
            component.setColour();

            expect(component.colour).to.equal(mockColour2.colour);
        });

        it('isMax() and IS', ()=> {
            component.barData = { confirmed: 100 };
            component.max = 100;
            expect(component.isMax()).to.equal(true);
        });

        it('isMax() and IS NOT', ()=> {
            component.barData = { confirmed: 4 };
            component.max = 100;
            expect(component.isMax()).to.equal(false);
        });


        it('setStyle()', ()=> {
            component.colour = 'green';
            component.width = 5; 
            component.style = {};

            component.setStyle();

            expect( component.style['background-color'] ).to.equal(component.colour);
            expect( component.style['width'] ).to.equal(component.width + '%');
        });
    });

    describe('Data Bar - Integration Tests', () => {
        const dataBarNameSelector = '.data-bar__name__value';
        it('should render', () => {
            const testData = { name: 'testName' };
            const wrapper = shallowMount(DataBar, {
                propsData: { barData: testData, max: 200 }
            })
            expect(wrapper.find('.data-bar__name__value').text()).contains(testData.name);
        });
    });
})



