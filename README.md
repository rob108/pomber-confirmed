# Pomber Confirmed Front End

Consumes Data from https://pomber.github.io/covid19/timeseries.json to display a simple count of confirmed Corona cases per country

Demo: https://competent-engelbart-d8e319.netlify.app/#/ 

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run dev
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
