describe('Countries example', () => {
    it('shows the data for test country 2', () => {
      const dataBarNameSelector = '.data-bar__name__value';
      const dataBarBar = '.data-bar__bar';
      const dataTimeout = 3000;
      cy.visit('/');
      cy.wait(dataTimeout);
      cy.get(dataBarBar + ':first').should('have.css', 'background-color').and('eq', 'rgb(254, 150, 119)');
    })
  })
  